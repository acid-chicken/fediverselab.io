
---
layout: "post"
title: "Mastodon 2.4.0 finally out"
date: 2018-05-23
tags:
    - mastodon
preview: "As always, new release has many additions and improvements. In terms of better privacy, you can now hide following / followers lists, if you wish."
url: "https://github.com/tootsuite/mastodon/releases/tag/v2.4.0"
lang: en
---

As always, new release has many additions and improvements. In terms of better privacy, you can now hide following / followers lists, if you wish. It also includes better offline support, performance improvements, RSS for users, admin UI improvements, support for running Mastodon as a hidden service, and much more.
Read full changelog [here](https://github.com/tootsuite/mastodon/releases/tag/v2.4.0).
