
---
layout: "post"
title: "Themed servers"
---

This is a list of themed servers. Currently it includes mostly [Mastodon](/en/mastodon) servers. This will change in future updates.
Servers are not restricted to their theme. They only help people with common interests find their community.

<ul class="article-list">

#### [Notable mention](#notable-mention)

* https://social.lab.cultura.gov.br - by FOSS lab of Brazilian Ministry of Culture
* https://mastodon.mit.edu - for the MIT community
* https://mastodon.acc.sunet.se - by Academic Computer Club at Umeå University, Sweden
* https://mastodon.iut-larochelle.fr - by University of La Rochelle
* https://mastodon.redbrick.dcu.ie - by Redbrick, computer networking society of Dublin City University
* https://mastodonevry.ovh - for l'Université d'Evry-Val-d'Essonne students
* https://mastodon.etalab.gouv.fr - for French public servants who are involved in innovative projects

#### [Run by tech-savvy organizations](#run-by-tech-savvy-organizations)

* https://en.osm.town - for the OpenStreetMap Community
* https://fr.osm.social - for francophone OpenStreetMap Community
* https://chaos.social - CCC related
* https://framapiaf.org - by Framasoft, association promoting FOSS, providing privacy-respecting services
* https://mastodon.dotopia.dk - by DO:TOPIA, Denmark based tech activism collective
* https://mastodon.hackerlab.fr - by Hackerlab
* https://mi.pede.rs - by Hacklab-in-Mama hackerspace, Zagreb
* https://social.weho.st - by WeHost, non-profit internet service provider, Amsterdam
* https://m.g3l.org - by G3L, libre software association
* https://mastodon.nzoss.nz - by New Zealand Open Source Society
* https://mamot.fr - by Quadrature du Net, association for the defence of digital rights and freedoms
* https://mastodon.roflcopter.fr - by Roflcopter, [CHATONS](https://chatons.org) member
* https://mastodon.zaclys.com - by Zaclys, association offering francophone privacy-respecting services
* https://social.buffalomesh.net - by Buffalomesh, mesh network in Buffalo, NY
* https://social.chinwag.im - by Chinwag project
* https://eunivers.social - by Eunivers online edition
* https://mastodon.freifunk-muensterland.de - by Freifunk Muensterland
* https://unixcorn.xyz - by Unixcorn

#### [Servers for techies](#servers-for-techies)
* https://infosec.exchange - infosec and IT security discussions
* https://naos.crypto.church - powers of cryptography and cryptanalysis
* https://zerohack.xyz - hacking centric: programming, Linux, BSD, Ethics, whatever
* https://geeks.one - for humans and geeks
* https://mastodon.technology - for people interested in technology
* https://fosstodon.org - dedicated to Free and Open Source Software
* https://floss.social - for people who support or build Free Libre Open Source Software
* https://tuxspace.net - for Linux lovers, users, sysadmins, and the like
* https://mastodon.horde.net.br - to bring together enthusiasts of free software
* https://miaou.drycat.fr - for geeks, FOSS and cat lovers, [CHATONS](https://chatons.org) member
* https://mastodon.partecipa.digital - conversations about IT, Open Source, Linux
* https://pao.moe - cat, FOSS, nix
* https://linuxrocks.online - dedicated to Linux and technologies
* https://functional.cafe - for people interested in functional programming and languages
* https://social.csswg.org - for anyone interested in CSS or general web development
* https://oldbytes.space - for old hardware fans
* https://mathstodon.xyz -  for Maths people
* https://cb.ku.cx - all things Technology, hacking, security and reverse engineering
* https://bsd.network - for *BSD users and devs
* https://fasterwhen.red - automotive- and engineering-related topics
* https://recurrent.network - all things artificial neural network-related
* https://idevs.id - home to developer, programmer, coder
* https://vpn.tv - for VPN operators, users and enthusiasts

#### [Gamedev](#gamedev)
* https://mastodon.gamedev.place - gamedev and related professions
* https://elekk.xyz - by gamers, for gamers
* https://mastodon.sergal.org - general trends toward tech, games, and everything fluffy
* https://robloxcommunity.social - for those part of or interested in the ROBLOX and RBXDev community
* https://social.noff.co - for tech, gaming, and good times
* https://unityjp-mastodon.tokyo - Unity Japan
* https://gonext.gg - esports-centric with focus on competitive gaming
* https://gamemaking.social - amateur videogame making: game writers, game players welcome

#### [Entertainment](#entertainment)
* https://tenforward.social - theme Star Trek
* https://moseisley.club - theme Star Wars
* https://wowsdon.xyz - theme World of WarShips
* https://kirakiratter.com - for Aikatsu fans
* https://donphan.social - for Pokémon enthusiasts
* https://inkopolis.cafe - for Splatoon 2 players

#### [Humanities](#humanities)
* https://mastodon.art - for artists
* https://mastodon.cc - for art
* https://paintacu.be - painting community
* https://artalley.porn - for artists and commissioners
* https://baraag.net - freely share all types of art, uncensored, 18+
* https://bookwor.ms - for book-lovers
* https://booktoot.club - for sharing and discussing books
* https://www.bookbeezhive.com - for authors and their readers
* https://writing.exchange - community for poets, writers, bloggers
* https://wandering.shop - science fiction and fantasy community
* https://imaginair.es - for writers, photographers, painters, cartoonists and people with imagination
* https://social.nasqueron.org - for creative people, writers, developers and thinkers
* https://bookwitty.social - dedicated to discovering books
* https://vis.social - for anyone into data, visualization, creative coding, related arts and research
* https://photog.social - place for your photos
* https://music.glitch.pizza - dedicated to talking about and sharing music
* https://linernotes.club - for fans of recorded music
* https://ausglam.space - for Australian galleries, libraries, archives, museums and records people
* https://spooky.pizza - for linguists and linguist-adjacent people

#### [Sciences](#sciences)
* https://scholar.social - for researchers, undergrads, journal editors, librarians, administrators
* https://scicomm.xyz - discussions for scientists and science enthusiasts
* https://mastodon.huma-num.fr - dedicated to the higher education and research
* https://tusk.schoollibraries.net - to help educators improve the School Libraries Resource Network
* https://qoto.org -  for scholars in Science, Technology, Engineering, and Mathematics (STEM)

#### [Hobbies](#hobbies)
* https://makestuff.club - for people that make stuff
* https://pod.social - for podcast creators, listeners, and enthusiasts
* https://mhz.social - for radio amateurs, DXers and anyone interested to geek out about radio
* https://smusi.ch - for musicians, by musicians
* https://metalhead.club - for metal friends
* https://electro.social - for electronic music enthusiasts, and electro music makers
* https://mastodon.flights - focused on travel, world affairs and jet setting lifestyles
* https://boardgames.social - for board gamers and the games they love
* https://pipes.social - hobby of pipe collecting and pipe smoking
* https://mastodon.beerfactory.org - for beermakers
* https://mastd.racing - dedicated to motosports
* https://2ndamendment.social - have the freedom to discuss, share and show off your guns
* https://onthesecond.com - for those who Love liberty, guns, and independence
* https://digipres.club - conversations about digital preservation
* https://newtype.institute - anime-centric community
* https://weebs.moe - anime, manga
* https://occult.camp - focused on spitituality, witchcraft and various shades of woo
* https://witchcraft.cafe - for witches
* https://witchey.club - for anyone curious about witchcraft
* https://talesofterror.eu - for fans of horror literature
* https://hodl.social - place to discuss Stellar, cryptocurrencies and blockchain

#### [Languages](#languages)
* https://esperanto.masto.host - for those interested in Esperanto
* https://masto.pt - for people speaking Portugese
* https://tooot.im - main instance language is Hebrew
* https://persadon.com - for Persian speaking folks
* https://the.wired.sehol.se - for Hungarian speakers
* https://toot.si - Slovenian (multilingual) instance
* https://tootcn.com - Chinese instance
* https://mamut.social - primarily for Spanish speaking people

#### [Regional](#regional)
* https://bonn.social - Bonn, Germany
* https://toot.berlin - Berlin, Germany
* https://muenster.im - Münsterland, Germany
* https://ruhr.social - Ruhr area, Germany
* https://ps.s10y.eu - like-minded people in Brussels, Belgium
* https://mastoton.fi - Finland
* https://snabeltann.no - Norway
* https://www.sosial.eu - Norway
* https://mastodon.se - Sweden
* https://gr.social - Greece
* https://babelut.be - Belgium
* https://mastodont.cat - Catalonia
* https://occitanie.social - Occitania
* https://bne.social- Queensland, Australia
* https://mastodon.uy - Uruguay
* https://mastodon.scot - Scotland, or who identify as Scottish
* https://mastodon.me.uk - UK tech community
* https://manx.social - the Isle of Man
* https://masto.donte.com.br - Brazil
* https://mastodon.floripa.br - companies and startups in Florianópolis, Brazil
* https://mastodonchile.club - users latinos, especially from Chile
* https://pnw.social - citizens of Washington, Oregon, Idaho, Western Montana, and Alaska
* https://riverwest.xyz - Riverwest neighborhood of Milwaukee, Wisconsin, USA
* https://toot.memtech.website - Memphis tech community, USA
* https://techandbeer.social - tech and beer in Orlando, USA
* https://valleypost.us - residents and stakeholders in the Valley, USA
* https://mspsocial.net - Twin Cities of Minneapolis and St. Paul, Minnesota, USA
* https://kokomo.space - community of Kokomo city, Indiana, USA
* https://opensocial.africa - for Africa, by Africans
* https://polska.masto.host - Poland

#### [Political and social views](#political-and-social-views)
* https://todon.nl - for leftists, socialists, anarchists, Marxists, social democrats, social liberals
* https://hispagatos.space - for hackers, social anarchists, and anarchist hackers
* https://libertarian.chat - a community driven instance for people who love liberty
* https://voluntary.world - for voluntaryists, anarcho-capitalists, non-aggression centered activists
* https://anarchism.space - for social anarchists
* https://insoumis.social - for comrades, combatants, fighters
* https://sosh.network - for freedom of thought, of opinion, of conversation
* https://roughseas.xyz - for libertarians, classical liberals, right-of-center thinkers, gun owners
* https://thepeanut.farm - for center-leftists worldwide
* https://sunbeam.city - focusing on the solarpunk aesthetic (anarchist, anti-capitalist, utopian)
* https://anticapitalist.party
* https://cheeky.red

#### [Economics, business](#economics-business)
* https://social.outsourcedmath.com - discussing finance and investing
* https://hiddan.net - a safe space for entrepreneurs

#### [Ecology, animal lovers](#ecology-animal-lovers)
* https://mastodon.latransition.org - francophone movement for ecological transition
* https://onerescue.org - for animal lovers, and dedicated to helping rescue animals in need
* https://toot.cat
* https://pets-in.space

#### [Safe spaces](#safe-spaces)
* https://trans.town - all trans people welcome
* https://queer.party - for queer folk and non-queer folk alike
* https://queer.town - space for queer people to be positive and have fun
* https://social.wxcafe.net - for social justice activists, LGBTQIA+ people
* https://lgbt.io - for LGBT+ and allies
* https://tech.lgbt - for tech workers, academics, students, and others in tech who are LGBTQA+ or allies
* https://instance.business - a safe place for nonwhite people on the fediverse
* https://pipou.academy - emphasis on kindness
* https://biscuit.town - feminist movement and queer/lgbt+
* https://eldritch.cafe - for queer, feminist and anarchist people

#### [For people with a sense of self-irony](#for-people-with-a-sense-of-self-irony)
* https://shitter.me - the sky is the limit
* https://i.write.codethat.sucks
* https://sleeping.town - for people who like to sleep
* https://bibeogaem.zone - "nice place for silly people"
* https://society.oftrolls.com - mild trolling encouraged, but not required
* https://ephemeral.glitch.social - toots are ephemeral and disappear after a while
* https://dolphin.town - instance where you can only say the letter 'e'
* https://botsin.space - for bots

#### [Characters](#characters)
* https://dragon.style - for dragons, and people who like dragons
* https://monsterpit.net - for monsters, demons, aliens, strange creatures, and those who love them
* https://equestria.social - for pony fans

#### [Subcultures](#subcultures)
* https://yiff.rocks - for furries, by furries
* https://pridelands.io - for furries who want a place where they can speak like adults

#### [Notable generalistic](#notable-generalistic)
* https://projectx.fun - will be open for up to 100 users, for sanity and sustainability
* https://nulled.red - refuses to block or silence any instances
* https://waifu.one - no censorship, no instances are blocked
* https://ieji.de - no logs, has Tor .onion address
* https://inscope.social - for those who help to get us onto the best possible future
* https://tootplanet.space - beautiful landing
* https://assortedflotsam.com - instance committed to truth in discourse
* https://bigdoinks.online - a small community for jokes, goofs, and bits
* https://social.wiuwiu.de - powered by 100% clean energy
* https://mstdn.fr - by sysadmins, with a goal to provide robust experience for tens of thousands of users
* https://sn.angry.im - wise recommendation to set up own node, if possible, for healthy ecosystem
* https://scifi.fyi - runs glitch-soc, a version of Mastodon with experimental new features
* https://mastodonten.de - an instance for nice people
* https://mstdn.gnous.eu - instance francophone
* https://mcr.cool - for good and fun times
* https://7nw.eu - always running on the last tagged release
* https://colloportus.me - leave big social media and get back to social networking
* https://social.openalgeria.org
* https://social.datensturm.net
* https://mastodon.fun
* https://nicepeople.social
* https://trillion.social
* https://toot.chat
* https://tootme.de
* https://kyot.me

#### Disclaimer
Following instances skipped: closed registration *at the moment of checking*, running old code, description not understood due to language barrier or scarce description, long blocklists, experimental servers.

Add your own themed instance via [merge request](https://gitlab.com/distributopia/masto-world-overview). If your server already has many users, it will be nice of you to let other servers grow, for a healthier decentralization.
</ul>

## Other research links
- [Friendica world overview](https://gitlab.com/distributopia/friendica-world-overview)
- [Fediverse pirate servers](https://gitlab.com/distributopia/caramba)
- [Mastodon server distribution](https://chaos.social/@leah/99837391793032137) research by Leah
